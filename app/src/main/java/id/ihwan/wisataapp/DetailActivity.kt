package id.ihwan.wisataapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import org.jetbrains.anko.toast

class DetailActivity : AppCompatActivity() {

    private var nama:String = ""
    private var desa:String = ""
    private var kecamatan:String = ""
    private var jenis:String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val intent = intent

        nama = intent.getStringExtra("nama")
        desa = intent.getStringExtra("desa")
        kecamatan = intent.getStringExtra("kecamatan")
        jenis = intent.getStringExtra("jenis")

        toast("Mantap di ${desa}, ada ${nama}, sebuah ${jenis} di ${kecamatan}" )

    }
}
