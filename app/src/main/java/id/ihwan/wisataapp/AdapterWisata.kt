package id.ihwan.wisataapp

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import id.ihwan.wisataapp.BuildConfig.IMAGE_URL
import kotlinx.android.synthetic.main.item_list.view.*


/**
 * Created by Ihwan ID on 18,October,2018.
 * Subscribe my Youtube Channel => https://www.youtube.com/channel/UCjntzibNSsjjIOh0HoP9vxw
 * mynameisihwan@gmail.com
 */

class AdapterWisata(val context: Context, val items: ArrayList<Wisata>, private val listener: (Wisata) -> Unit):
        RecyclerView.Adapter<AdapterWisata.ViewHolder>(){



    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_list, p0, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.bindItem(items[p1], listener)

    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){

        fun bindItem(items: Wisata, listener: (Wisata) -> Unit){

            itemView.namaWisata.text = items.nama
            itemView.lokasiWisata.text = items.kecamatan
            Log.d("TEMPATHEHE", items.alamat)

            itemView.cardView.setOnClickListener {
                listener(items)
            }
            //gambar masih bermasalah
//            Picasso.get().load(IMAGE_URL+items.gambar).into(itemView.gambarWisata)
//            Picasso.get().load("https://upload.wikimedia.org/wikipedia/en/thumb/7/7e/Patrick_Star.png/220px-Patrick_Star.png").into(itemView.gambarWisata)
            Glide.with(itemView.context).load(IMAGE_URL+items.gambar).into(itemView.gambarWisata)
            Log.d("URL --> ", IMAGE_URL+items.gambar)
        }
    }
}